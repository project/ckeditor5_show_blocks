import { showBlocks } from '@ckeditor/ckeditor5-show-blocks';

export default {
  showBlocks,
};
